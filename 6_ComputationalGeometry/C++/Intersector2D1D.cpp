#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
  toleranceParallelism = 1.0E-7;
  toleranceIntersection = 1.0E-7;
  intersectionType = Intersector2D1D::NoInteresection;
  intersectionParametricCoordinate = 0.0;
}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  planeNormalPointer = &planeNormal;
  planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  lineOriginPointer = &lineOrigin;
  lineTangentPointer = & lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  // let us define PI: N (X-X_0) = 0 ;  r: X = P_0 + s T
  double planeNormalDotLineTangent = (*planeNormalPointer).dot(*lineTangentPointer);  // = N T
  double planeNormalDotRelativePosition = *planeTranslationPointer - (*planeNormalPointer).dot(*lineOriginPointer); // = N (X_0 - P_0)
  bool intersection = false;

  if(abs(planeNormalDotLineTangent) >= toleranceParallelism)  // if the plane and the line are not parallel
  {
    if(abs(planeNormalDotRelativePosition) >= toleranceIntersection)
      intersectionParametricCoordinate = planeNormalDotRelativePosition/planeNormalDotLineTangent;
    intersectionType = PointIntersection;
    intersection = true;
  }
  else if(abs(planeNormalDotRelativePosition) <= toleranceIntersection)  // if the line is contained in the plane
  {
    intersectionType = Coplanar;
  }

  return intersection;
}
