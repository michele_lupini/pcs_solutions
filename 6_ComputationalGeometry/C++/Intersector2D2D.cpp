#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
  toleranceParallelism = 1.0E-5;
  toleranceIntersection = 1.0E-7;
  intersectionType = Intersector2D2D::NoInteresection;
  pointLine.setZero(3);
  tangentLine.setZero(3);
  rightHandSide.setZero(3);
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  matrixNomalVector.row(0) = planeNormal;
  matrixNomalVector.row(0).normalize();  // set normalized first plane normal
  rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  matrixNomalVector.row(1) = planeNormal;
  matrixNomalVector.row(1).normalize();  // set normalized second plane normal
  rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
  double tangentLineNorm;
  intersectionType = NoInteresection;
  bool intersection = false;

  // compute the normalized vector, normal to the two planes
  matrixNomalVector.row(2) = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
  tangentLineNorm = matrixNomalVector.row(2).norm();
  matrixNomalVector.row(2) /= tangentLineNorm;

  double check = toleranceParallelism * toleranceParallelism * matrixNomalVector.row(0).squaredNorm() * matrixNomalVector.row(1).squaredNorm();
  if(tangentLineNorm * tangentLineNorm >= check)  // if planes are not parallel
  {
    // define the vector parallel to the intersection line
    tangentLine = matrixNomalVector.row(2);
    // define a point on the intersection line, by solving the linear system
    pointLine = matrixNomalVector.fullPivLu().solve(rightHandSide);
    intersectionType = LineIntersection;
    intersection = true;
  }
  else if(fabs(rightHandSide(0) - rightHandSide(1)) <= toleranceIntersection)  // if planes coincide
  {
    intersectionType = Coplanar;
  }

  return intersection;
}
