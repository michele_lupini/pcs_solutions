#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
    return M_PI*sqrt(2*(POW2(_a) + POW2(_b))); //perimeter approximation
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
    points.reserve(3);
    AddVertex(p1);
    AddVertex(p2);
    AddVertex(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    for(int i = 0; i < 3; i++)
      perimeter += (points[(i+1)%3]-points[i]).ComputeNorm2();

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    points.reserve(3);
    AddVertex(p1);
    AddVertex(Point(p1.X+edge, p1.Y));
    AddVertex(Point(p1.X+0.5*edge, p1.Y+0.5*sqrt(3)*edge));
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
    points.reserve(4);
    AddVertex(p1);
    AddVertex(p2);
    AddVertex(p3);
    AddVertex(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for(int i = 0; i < 4; i++)
      perimeter += (points[(i+1)%4]-points[i]).ComputeNorm2();

    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
    points.reserve(4);
    AddVertex(p1);
    AddVertex(Point(p1.X+base, p1.Y));
    AddVertex(Point(p1.X+base, p1.Y+height));
    AddVertex(Point(p1.X, p1.Y+height));
  }

  double Point::ComputeNorm2() const
  {
    return sqrt(POW2(X) + POW2(Y));
  }

  Point Point::operator+(const Point& point) const
  {
      Point tempPoint;
    tempPoint.X = X + point.X;
    tempPoint.Y = Y + point.Y;

    return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
    Point tempPoint;
    tempPoint.X = X - point.X;
    tempPoint.Y = Y - point.Y;

    return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
    X -= point.X;
    Y -= point.Y;

    return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
    X += point.X;
    Y += point.Y;

    return *this;
  }

  ostream &operator<<(ostream &stream, const Point &point)
  {
      stream << "Point: x = " << point.X << " y = " << point.Y << endl;
      return stream;
  }
}
