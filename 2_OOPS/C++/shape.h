#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X = 0.0; Y = 0.0; }
      Point(const double& x,
            const double& y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      int _a;
      int _b;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const { return Ellipse::Area(); }
  };


  class Triangle : public IPolygon
  {
    protected:
      Point _p1, _p2, _p3;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const { return Triangle::Area(); }
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1, _p2, _p3, _p4;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const { return Quadrilateral::Area(); }
  };

  class Rectangle : public Parallelogram
  {
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const { return Parallelogram::Area(); }
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const { return Rectangle::Area(); }
  };
}

#endif // SHAPE_H
