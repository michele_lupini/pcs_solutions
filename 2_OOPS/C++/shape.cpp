#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Point::Point(const double &x, const double &y)
  {
    X = x;
    Y = y;
  }

  Point::Point(const Point &point)
  {
    X = point.X;
    Y = point.Y;
  }

  Ellipse::Ellipse(const Point &center,
                   const int &a,
                   const int &b)
  {
    _center = center;
    _a = a;
    _b = b;
  }

  double Ellipse::Area() const
  {
    return _a * _b * M_PI;
  }

  Circle::Circle(const Point &center,
                 const int &radius) :
      Ellipse(center, radius, radius) { }

  Triangle::Triangle(const Point &p1,
                     const Point &p2,
                     const Point &p3)
  {
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
  }

  double Triangle::Area() const
  {
    // Area = 0.5*||(p2-p1)x(p3-p1)||
    return 0.5 * abs(_p1.X*_p2.Y - _p2.X*_p1.Y + _p2.X*_p3.Y -
                     _p3.X*_p2.Y + _p3.X*_p1.Y - _p1.X*_p3.Y);
  }

  TriangleEquilateral::TriangleEquilateral(const Point &p1,
                                           const int &edge) :
        Triangle(p1, Point(p1.X+edge, p1.Y), Point(p1.X+(0.5*edge), p1.Y+(edge*sqrt(3)/2))) { }

  Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
  {
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
  }

  double Quadrilateral::Area() const
  {
    // assuming ordered point, Area = 0.5*||(p2-p1)x(p3-p1)|| + 0.5||(p3-p1)x(p4-p1)||
    return 0.5*abs(_p1.X*_p2.Y + _p2.X*_p3.Y + _p3.X*_p4.Y +
                   _p4.X*_p1.Y - _p2.X*_p1.Y - _p3.X*_p2.Y -
                   _p4.X*_p3.Y - _p1.X*_p4.Y);
  }

  Parallelogram::Parallelogram(const Point &p1,
                               const Point &p2,
                               const Point &p4) :
      Quadrilateral(p1, p2, Point(p2.X+p4.X-p1.X, p2.Y+p4.Y-p1.Y), p4) { }

  Rectangle::Rectangle(const Point &p1,
                       const int &base,
                       const int &height) :
      Parallelogram(p1, Point(p1.X+base, p1.Y), Point(p1.X, p1.Y+height)) { }

  Square::Square(const Point &p1,
                 const int &edge) :
      Rectangle(p1, edge, edge) { }

}
