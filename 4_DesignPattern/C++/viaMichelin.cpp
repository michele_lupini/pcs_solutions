# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
  int RoutePlanner::BusAverageSpeed = 50;  // (km/h)

  void BusStation::ResetStation()
  {
    _numberBuses = 0;
    _buses.clear();
  }

  void BusStation::Load()
  {
    ResetStation();

    /// Open File
    ifstream file;
    file.open(_busFilePath.c_str());

    if(file.fail())
      throw runtime_error("Something goes wrong");

    /// Load Buses
    try
    {
      string line;

      /// Get number of buses
      getline(file, line); // Skip comment line
      getline(file, line);
      istringstream convertNumberBus;
      convertNumberBus.str(line);
      convertNumberBus >> _numberBuses;

      /// Fill buses
      getline(file, line); // Skip comment line
      getline(file, line); // Skip comment line
      _buses.resize(_numberBuses);
      for(int b = 0; b < _numberBuses; b++)
      {
        Bus& bus = _buses[b];

        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> bus.Id >> bus.FuelCost;
      }

      /// Close File
      file.close();
    }
    catch (exception)
    {
      ResetStation();
      throw runtime_error("Something goes wrong");
    }
  }

  const Bus &BusStation::GetBus(const int &idBus) const
  {
    if(idBus > _numberBuses)
      throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
  }

  void MapData::ResetMap()
  {
    _numberBusStops = 0;
    _numberStreets = 0;
    _numberRoutes = 0;
    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routesStreets.clear();
  }

  void MapData::Load()
  {
    ResetMap();

    /// Open File
    ifstream file;
    file.open(_mapFilePath);

    if(file.fail())
      throw runtime_error("Something goes wrong");

    /// Load Map
    try
    {
      string line;

      /// Get bus stops
      getline(file, line); // Skip comment line
      getline(file, line);
      istringstream convertNumberBusStops;
      convertNumberBusStops.str(line);
      convertNumberBusStops >> _numberBusStops;

      getline(file,line); //Skip comment line
      _busStops.resize(_numberBusStops);
      for(int bs = 0; bs < _numberBusStops; bs++)
      {
        BusStop& busStop = _busStops[bs];

        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> busStop.Id >> busStop.Name >> busStop.Latitude >> busStop.Longitude;
      }

      /// Get streets
      getline(file, line); // Skip comment line
      getline(file, line);
      istringstream convertNumberStreets;
      convertNumberStreets.str(line);
      convertNumberStreets >> _numberStreets;

      getline(file, line); // Skip comment line
      _streets.resize(_numberStreets);
      _streetsFrom.resize(_numberStreets);
      _streetsTo.resize(_numberStreets);
      for(int s = 0; s < _numberStreets; s++)
      {
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
      }

      /// Get routes
      getline(file, line); // Skip comment line
      getline(file, line);
      istringstream convertNumberRoutes;
      convertNumberRoutes.str(line);
      convertNumberRoutes >> _numberRoutes;

      getline(file, line); // Skip comment line
      _routes.resize(_numberRoutes);
      _routesStreets.resize(_numberRoutes);
      for(int r = 0; r < _numberRoutes; r++)
      {
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _routes[r].Id >> _routes[r].NumberStreets;

        _routesStreets[r].resize(_routes[r].NumberStreets);
        for(int s = 0; s < _routes[r].NumberStreets; s++)
          converter >> _routesStreets[r][s];
      }

      /// Close File
      file.close();
    }
    catch (exception)
    {
      ResetMap();
      throw runtime_error("Something goes wrong");
    }
  }

  const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
  {
    if(idRoute > _numberRoutes)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    if(streetPosition >= _routes[idRoute - 1].NumberStreets) // streetPosition starts from 0
      throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routesStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
  }

  const Route &MapData::GetRoute(const int &idRoute) const
  {
    if(idRoute > _numberRoutes)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute -1];
  }

  const Street &MapData::GetStreet(const int &idStreet) const
  {
    if(idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
  }

  const BusStop &MapData::GetStreetFrom(const int &idStreet) const
  {
    if(idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idFrom = _streetsFrom[idStreet - 1];

    return _busStops[idFrom - 1];
  }

  const BusStop &MapData::GetStreetTo(const int &idStreet) const
  {
    if(idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idTo = _streetsTo[idStreet - 1];

    return _busStops[idTo - 1];
  }

  const BusStop &MapData::GetBusStop(const int &idBusStop) const
  {
    if(idBusStop > _numberBusStops)
      throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
  }

  string MapViewer::ViewRoute(const int &idRoute) const
  {
    const Route& route = _mapData.GetRoute(idRoute);

    ostringstream routeView;
    routeView << to_string(idRoute) + ": ";

    for(int s = 0; s < route.NumberStreets; s++)
    {
      int street = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(street).Name;
      routeView << from + " -> ";

      if(s == route.NumberStreets - 1)
      {
        string to = _mapData.GetStreetTo(street).Name;
        routeView << to;
      }
    }

    return routeView.str();
  }

  string MapViewer::ViewStreet(const int &idStreet) const
  {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
  }

  string MapViewer::ViewBusStop(const int &idBusStop) const
  {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude/10000.0) + ", " + to_string((double)busStop.Longitude/10000.0) + ")";
  }

  int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
  {
    const Route& route = _mapData.GetRoute(idRoute);
    int travelTime = 0; // (s)

    for(int s = 0; s < route.NumberStreets; s++)
      travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime;
  }

  int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
  {
    const Bus& bus = _busStation.GetBus(idBus);
    const int totalTravelTime = ComputeRouteTravelTime(idRoute);

    double routeCost = (totalTravelTime / 3600.0) * BusAverageSpeed * bus.FuelCost;

    return (int)routeCost;
  }

}
