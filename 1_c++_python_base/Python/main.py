import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    # open file
    file = open(inputFilePath, 'r')

    # get text for encryption
    text = file.readline().strip('\n')  # text is in one line (requirement)

    # close file
    file.close()

    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    i: int = 0  # text counter
    j: int = 0  # password counter
    lenText: int = len(text)  # length of text
    lenPassword: int = len(password)  # length of password
    encryptedText = ''  # define empty string

    '''
        thanks to the restriction on password (only uppercase letters),
        there is not the ambiguity problem,
        so the encrypted text will be well defined
    '''

    for i in range(lenText):  # cycle on text
        tempChar = (ord(text[i]) + ord(password[j]))  # temporary ASCII encryption
        if tempChar > 126:  # no need of cycle
            tempChar -= 94  # if out of ASCII range [32-126], go back to 32nd char

        encryptedText += chr(tempChar)  # encrypt text

        j += 1  # next letter on password
        if j == lenPassword:  # reset password counter
            j = 0

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    i: int = 0  # text counter
    j: int = 0  # password counter
    lenText: int = len(text)  # length of text
    lenPassword: int = len(password)  # length of password
    decryptedText = ''  # define empty string

    for i in range(lenText):  # cycle on text
        tempChar = ord(text[i]) - ord(password[j])  # temporary ASCII decryption
        if tempChar < 32:  # no need of cycle
            tempChar += 94  # if out of ASCII range [32-126], go on to 126th char

        decryptedText += chr(tempChar)  # decrypt text

        j += 1  # next letter on password
        if j == lenPassword:  # reset password counter
            j = 0

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text= ", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
