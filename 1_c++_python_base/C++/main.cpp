#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl; //added functionc_str()

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
  /// Open file
  ifstream file;
  file.open(inputFilePath);

  if (file.fail())
  {
    cerr<< "file open failed"<< endl;
    return false;
  }

  /// Get text for encryption
  getline(file,text); //takes one line of text (requirement)

  /// Close file
  file.close();

  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
  int i=0; //text counter
  int j=0; //password counter
  int lenText=text.size(); //length of text
  int lenPassword=password.size(); //length of password
  int tempChar; //temporary ASCII encryption
  encryptedText.resize(lenText); //initialize string

  /*
    thanks to the restriction on password (only uppercase letters),
    there is not the ambiguity problem,
    so the encrypted text will be well defined
  */

  while(i<lenText)
  {
    tempChar=text[i]+password[j]; //encryption by sum of ASCII code
    if(tempChar>126) //no need of cycle
      tempChar-=94; //if out of ASCII range, goes back to 32nd char

    encryptedText[i]=tempChar; //encrypt text

    i++; //next letter on text
    j++;

    if(j==lenPassword) //reset password counter
      j=0;
  }

  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    int i=0; //text counter
    int j=0; //password counter
    int lenText=text.size(); //length of text
    int lenPassword=password.size(); //length of password
    int tempChar; //temporary ASCII encryption
    decryptedText.resize(lenText); //initialize string

    while(i<lenText)
    {
      tempChar=text[i]-password[j]; //decryption by sum of ASCII code
      if(tempChar<32) //no need of cycle
        tempChar+=94; //if out of ASCII range, go on to 126th char

      decryptedText[i]=tempChar; //decrypt text

      i++; //next letter on text
      j++;

      if(j==lenPassword) //reset password counter
        j=0;
    }

  return  true;
}
