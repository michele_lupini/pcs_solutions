#include "Pizzeria.h"

namespace PizzeriaLibrary {

  Ingredient::Ingredient(const string &name, const string &description, const int &price)
  {
    Name = name;
    Description = description;
    Price = price;
  }

  void Pizza::AddIngredient(const Ingredient &ingredient)
  {
    _ingredients.push_back(ingredient);
  }

  int Pizza::NumIngredients() const
  {
    return _ingredients.size();
  }

  int Pizza::ComputePrice() const
  {
    int price=0;
    for(list<Ingredient>::const_iterator it = _ingredients.cbegin(); it != _ingredients.cend(); ++it)
      price += it->Price;
    return price;
  }

  void Order::InitializeOrder(int numPizzas)
  {
    _pizzas.reserve(numPizzas);
  }

  void Order::AddPizza(const Pizza &pizza)
  {
    _pizzas.push_back(pizza);
  }

  const Pizza &Order::GetPizza(const int &position) const
  {
    if(NumPizzas() < (position - 1)) //position starts from 1 to the total number of pizzas
      throw runtime_error("Position passed is wrong");
    return _pizzas[position - 1];
  }

  int Order::NumPizzas() const
  {
    return _pizzas.size();
  }

  int Order::ComputeTotal() const
  {
    int total = 0;
    int numberPizzas = NumPizzas();
    for(int i = 0; i < numberPizzas; i++)
      total += _pizzas[i].ComputePrice();
    return total;
  }

  void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
  {
    if(_listIngredients.find(name) != _listIngredients.end())
      throw runtime_error("Ingredient already inserted");
    _listIngredients.insert ( std::pair<string,Ingredient>(name, Ingredient(name, description, price)) );
  }

  const Ingredient &Pizzeria::FindIngredient(const string &name) const
  {
    map<string,Ingredient>::const_iterator it = _listIngredients.find(name);
    if(it == _listIngredients.end())
      throw runtime_error("Ingredient not found");
    return it->second;
  }

  void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
  {
    if(_menuPizzas.find(name) != _menuPizzas.end())
      throw runtime_error("Pizza already inserted");

    Ingredient ingredient;
    Pizza pizza;
    int numberIngredients = ingredients.size();
    for(int i = 0; i < numberIngredients; i++)
    {
      ingredient = FindIngredient(ingredients[i]);
      pizza.Pizza::AddIngredient(ingredient);
    }
    pizza.Name = name;
    _menuPizzas.insert ( std::pair<string,Pizza>(name, pizza) );
  }

  const Pizza &Pizzeria::FindPizza(const string &name) const
  {
    map<string,Pizza>::const_iterator it = _menuPizzas.find(name);
    if(it == _menuPizzas.end())
      throw runtime_error("Pizza not found");
    return it->second;
  }

  int Pizzeria::CreateOrder(const vector<string> &pizzas)
  {
    int numberPizzas = pizzas.size();
    if(numberPizzas == 0)
      throw runtime_error("Empty order");

    Pizza pizza;
    int numOrder = _orders.size();
    _orders.resize(numOrder + 1);
    _orders[numOrder].InitializeOrder(numberPizzas);
    for(int i = 0; i < numberPizzas; i++)
    {
      pizza = FindPizza(pizzas[i]);
      _orders[numOrder].AddPizza(pizza);
    }
    return (numOrder + 1000); //order identifier, starts from 1000
  }

  const Order &Pizzeria::FindOrder(const int &numOrder) const
  {
    if((numOrder - 1000) >= int(_orders.size()))
      throw runtime_error("Order not found");
    return _orders[numOrder - 1000];
  }

  string Pizzeria::GetReceipt(const int &numOrder)
  {
    int index = numOrder - 1000;
    if(index >= int(_orders.size()))
      throw runtime_error("Order not found");

    string receipt = "";
    Pizza pizza;
    int numberPizzas = _orders[index].NumPizzas();
    for(int i = 0; i < numberPizzas; i++)
    {
      pizza = _orders[index].GetPizza(i+1);
      receipt += ("- " + pizza.Name + ", " + to_string(pizza.ComputePrice()) + " euro\n");
    }
    receipt += ("  TOTAL: " + to_string(_orders[index].ComputeTotal()) + " euro\n");
    return receipt;
  }

  string Pizzeria::ListIngredients() const
  {
    string listIngredients = "";
    for(map<string,Ingredient>::const_iterator it = _listIngredients.cbegin(); it != _listIngredients.cend(); ++it)
      listIngredients += (it->second.Name + " - '" + it->second.Description + "': " + to_string(it->second.Price) + " euro\n");
    return listIngredients;
  }

  string Pizzeria::Menu() const
  {
    string menu = "";
    for(map<string,Pizza>::const_reverse_iterator it = _menuPizzas.crbegin(); it != _menuPizzas.crend(); ++it)
      menu += (it->second.Name + " (" + to_string(it->second.NumIngredients()) + " ingredients): " + to_string(it->second.ComputePrice()) + " euro\n");
    return menu;
  }

}
