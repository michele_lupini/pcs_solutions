class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.__ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.__ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.__ingredients)

    def computePrice(self) -> int:
        price: int = 0
        for ingredient in self.__ingredients:
            price += ingredient.Price
        return price


class Order:
    def __init__(self):
        self.__pizzas = []

    def getPizza(self, position: int) -> Pizza:
        if self.numPizzas() < (position - 1):  # position starts from 1 to the total number of pizzas
            raise Exception('Position passed is wrong')
        return self.__pizzas[position - 1]

    def initializeOrder(self, numPizzas: int):
        self.__pizzas = [] * numPizzas

    def addPizza(self, pizza: Pizza):
        self.__pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.__pizzas)

    def computeTotal(self) -> int:
        total: int = 0
        for pizza in self.__pizzas:
            total += pizza.computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.__listIngredients = {}
        self.__menuPizzas = {}
        self.__orders = []

    def addIngredient(self, name: str, description: str, price: int):
        if name in self.__listIngredients:
            raise Exception('Ingredient already inserted')
        self.__listIngredients[name] = Ingredient(name, price, description)

    def findIngredient(self, name: str) -> Ingredient:
        if name not in self.__listIngredients:
            raise Exception('Ingredient not found')
        return self.__listIngredients[name]

    def addPizza(self, name: str, ingredients: []):
        if name in self.__menuPizzas.keys():
            raise Exception('Pizza already inserted')
        pizza = Pizza(name)
        ingredient: Ingredient
        for ingredientName in ingredients:
            ingredient = self.findIngredient(ingredientName)
            pizza.addIngredient(ingredient)
        pizza.Name = name
        self.__menuPizzas[pizza.Name] = pizza

    def findPizza(self, name: str) -> Pizza:
        if name not in self.__menuPizzas:
            raise Exception('Pizza not found')
        return self.__menuPizzas[name]

    def createOrder(self, pizzas: []) -> int:
        numberPizzas: int = len(pizzas)
        if numberPizzas == 0:
            raise Exception('Empty order')
        pizza: Pizza
        numOrder: int = len(self.__orders)
        self.__orders.append(Order())
        self.__orders[numOrder].initializeOrder(numberPizzas)
        for name in pizzas:
            pizza = self.findPizza(name)
            self.__orders[numOrder].addPizza(pizza)
        return numOrder + 1000  # order identifier, starts from 1000

    def findOrder(self, numOrder: int) -> Order:
        if (numOrder - 1000) >= len(self.__orders):
            raise Exception('Order not found')
        return self.__orders[numOrder - 1000]

    def getReceipt(self, numOrder: int) -> str:
        index: int = numOrder - 1000
        if index >= len(self.__orders):
            raise Exception('Order not found')
        receipt: str = ''
        pizza: Pizza
        numberPizzas: int = self.__orders[index].numPizzas()
        for i in range(numberPizzas):
            pizza = self.__orders[index].getPizza(i+1)
            receipt += ('- ' + pizza.Name + ', ' + str(pizza.computePrice()) + ' euro\n')
        receipt += ('  TOTAL: ' + str(self.__orders[index].computeTotal()) + ' euro\n')
        return receipt

    def listIngredients(self) -> str:
        ingredients: str = ''
        for ingredient in sorted(self.__listIngredients.values(), key=lambda x: x.Name):
            ingredients += (ingredient.Name + ' - \'' + ingredient.Description + '\': ' + str(ingredient.Price) + ' euro\n')
        return ingredients

    def menu(self) -> str:
        menu: str = ''
        for pizza in sorted(self.__menuPizzas.values(), key=lambda x: x.Name):
            menu += (pizza.Name + ' (' + str(pizza.numIngredients()) + ' ingredients): ' + str(pizza.computePrice()) + ' euro\n')
        return menu
